function calculate_age(dob) {
    const diff_ms = Date.now() - dob.getTime();
    const age_dt = new Date(diff_ms);

    return Math.abs(age_dt.getUTCFullYear() - 1970);
}


function setText() {
    let text =  `My name is Ben Blakemore, I am ${calculate_age(new Date(2003, 3, 3))} years old. 
    I am currently a developer on a Minecraft server called 30 Second Network, which has had a peak player count of 2313 players across 2 gamemodes (skyblock and factions).
    I have been programming since the age of 9.`;
    document.getElementById("about-me").innerText = text
}